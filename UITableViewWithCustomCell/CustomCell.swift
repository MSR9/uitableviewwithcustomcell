//
//  CustomCell.swift
//  UITableViewWithCustomCell
//
//  Created by EPITADMBP04 on 3/18/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    
    @IBOutlet weak var animalImage: UIImageView!
    @IBOutlet weak var animalLbl: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
