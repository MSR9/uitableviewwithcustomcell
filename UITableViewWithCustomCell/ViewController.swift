//
//  ViewController.swift
//  UITableViewWithCustomCell
//
//  Created by EPITADMBP04 on 3/18/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
 
    @IBOutlet weak var tableView: UITableView!
    
    let elements = ["Ant","Cat","Deer","Dog","Elephant","Frog","Goat","Hippo","Horse","Monkey","Owl","Parrot","Rabbit","Zebra"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell") as! CustomCell
        cell.cellView.layer.cornerRadius = cell.cellView.frame.height / 2
        cell.animalLbl.text = elements[indexPath.row].capitalized
        cell.animalImage.image = UIImage(named: elements[indexPath.row])
        cell.animalImage.layer.cornerRadius = cell.animalImage.frame.height / 2 
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
     
}


